﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace OthelloUI
{
    public static class FormManager
    {
        private static GameEventArgs s_GameArgs;

        internal static void start()
        {
            EntryForm entryForm = new EntryForm();
            entryForm.ModeSelectedClickOccured += modeSelectedButton_ClickOccured;
            entryForm.ShowDialog();
            if (entryForm.DialogResult == DialogResult.OK)
            {
                DialogResult boardResult;
                do
                {
                    OthelloGameBoardForm gameBoardForm = new OthelloGameBoardForm(s_GameArgs);
                    gameBoardForm.Shown += gameBoardForm.GameBoardForm_Shown;
                    boardResult = gameBoardForm.ShowDialog();
                }
                while (boardResult == DialogResult.OK);
            }
        }

        private static void modeSelectedButton_ClickOccured(object sender, EventArgs e)
        {
            s_GameArgs = (GameEventArgs)e;
        }
    }
}
