﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OthelloLogic;

namespace OthelloUI
{
    /// <summary>
    /// Class that Contains Main Menu Form
    /// </summary>
    /// <remarks>
    /// <see cref="Form"/>
    /// </remarks>
    internal sealed class EntryForm : Form
    {
        public event EventHandler ModeSelectedClickOccured;

        private readonly Button r_ButtonBoardSize = new Button();
        private readonly Button r_ButtonPlayerVsComputer = new Button();
        private readonly Button r_ButtonPlayerVsPlayer = new Button();
        private int m_BoardRib = 6;
        private bool m_IsPlayerVsPlayerMode;

        public EntryForm()
        {
            this.Text = "Othello - Game Settings";
            this.Size = new Size(450, 250);
            this.StartPosition = FormStartPosition.CenterScreen;
            this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
        }

        public int BoardRib
        {
            get { return m_BoardRib; }
            set { m_BoardRib = value; }
        }

        public bool IsPlayerVsPlayerMode
        {
            get { return m_IsPlayerVsPlayerMode; }
            set { m_IsPlayerVsPlayerMode = value; }
        }

        public Button ButtonPlayerVsComputer
        {
            get { return r_ButtonPlayerVsComputer; }
        }

        public Button ButtonPlayerVsPlayer
        {
            get { return r_ButtonPlayerVsPlayer; }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            initControlsEntryForm();
        }

        private void initControlsEntryForm()
        {
            const byte k_SideDeviation = 10;
            const byte k_UpDeviation = 20;

            r_ButtonBoardSize.Text = string.Format("Board Size: {0}x{0} (click to increase)", BoardRib);
            r_ButtonBoardSize.Font = new Font(r_ButtonBoardSize.Font.Name, r_ButtonBoardSize.Font.Size + 7);
            r_ButtonBoardSize.Location = new Point(k_SideDeviation, k_UpDeviation);
            r_ButtonBoardSize.Width = ClientSize.Width - (k_SideDeviation * 2);
            r_ButtonBoardSize.Height = (ClientSize.Height - (3 * k_UpDeviation)) / 2;
            r_ButtonBoardSize.Click += boardSizeButton_Clicked;
            r_ButtonBoardSize.MouseEnter += EntryMenuButton_MouseEnter;
            r_ButtonBoardSize.MouseLeave += EntryMenuButton_MouseLeave;

            ButtonPlayerVsComputer.Text = "Player Vs Computer";
            ButtonPlayerVsComputer.Font = new Font(ButtonPlayerVsComputer.Font.Name, r_ButtonBoardSize.Font.Size);
            ButtonPlayerVsComputer.Location = new Point(k_SideDeviation, (2 * k_UpDeviation) + r_ButtonBoardSize.Height);
            ButtonPlayerVsComputer.Width = (r_ButtonBoardSize.Width / 2) - (k_SideDeviation / 2);
            ButtonPlayerVsComputer.Height = r_ButtonBoardSize.Height;
            ButtonPlayerVsComputer.Click += modeButton_Clicked;
            ButtonPlayerVsComputer.MouseEnter += EntryMenuButton_MouseEnter;
            ButtonPlayerVsComputer.MouseLeave += EntryMenuButton_MouseLeave;

            r_ButtonPlayerVsPlayer.Text = "Player Vs Player";
            r_ButtonPlayerVsPlayer.Font = new Font(r_ButtonPlayerVsPlayer.Font.Name, ButtonPlayerVsComputer.Font.Size);
            r_ButtonPlayerVsPlayer.Location = new Point(ButtonPlayerVsComputer.Location.X + ButtonPlayerVsComputer.Width + k_SideDeviation, ButtonPlayerVsComputer.Location.Y);
            r_ButtonPlayerVsPlayer.Width = (r_ButtonBoardSize.Width / 2) - (k_SideDeviation / 2);
            r_ButtonPlayerVsPlayer.Height = r_ButtonBoardSize.Height;
            r_ButtonPlayerVsPlayer.Click += modeButton_Clicked;
            r_ButtonPlayerVsPlayer.MouseEnter += EntryMenuButton_MouseEnter;
            r_ButtonPlayerVsPlayer.MouseLeave += EntryMenuButton_MouseLeave;

            this.Controls.AddRange(new Control[] { r_ButtonBoardSize, ButtonPlayerVsComputer, r_ButtonPlayerVsPlayer });
        }

        private void boardSizeButton_Clicked(object sender, EventArgs e)
        {
            const int k_MinBoardSize = 6;
            const int k_MaxBoardSize = 12;

            BoardRib = BoardRib != k_MaxBoardSize ? BoardRib + 2 : k_MinBoardSize;
            ((Button)sender).Text = string.Format("Board Size: {0}x{0} (click to increase)", BoardRib);
        }

        /// <summary>
        /// Method that change Button color every time when mouse enter to the Button area
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EntryMenuButton_MouseEnter(object sender, EventArgs e)
        {
            ((Button) sender).BackColor = Color.Aquamarine;
        }

        /// <summary>
        /// Method that change Button color every time when mouse leave the Button area
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EntryMenuButton_MouseLeave(object sender, EventArgs e)
        {
            ((Button) sender).BackColor = DefaultBackColor;
        }

        private void modeButton_Clicked(object sender, EventArgs e)
        {
            if (sender == ButtonPlayerVsPlayer)
            {
                IsPlayerVsPlayerMode = true;
            }

            if (ModeSelectedClickOccured != null)
            {
                ModeSelectedClickOccured.Invoke(this, new GameEventArgs(BoardRib, IsPlayerVsPlayerMode));
            }

            GameEngine.SupportOteloBoard.BoardRibSize = (byte)BoardRib;
            GameEngine.SupportOteloBoard.GameBoardMatrix = new char[BoardRib + 2, BoardRib + 2];
            GameEngine.InitSupportBoard();
            this.DialogResult = DialogResult.OK;
        }
    }
}
