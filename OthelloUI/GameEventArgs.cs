﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OthelloUI
{  
    public class GameEventArgs : EventArgs
    {
        private readonly int r_BoardRib;
        private readonly bool r_IsModePlayerVsPlayer;

        public GameEventArgs(int i_BoardRib, bool i_IsModePlayerVsPlayer)
        {
            r_BoardRib = i_BoardRib;
            r_IsModePlayerVsPlayer = i_IsModePlayerVsPlayer;
        }

        public int BoardRib
        {
            get { return r_BoardRib; }
        }

        public bool IsModePvsP
        {
            get { return r_IsModePlayerVsPlayer; }          
        }
    }
}
