﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using OthelloLogic;

namespace OthelloUI
{
    public static class Program
    {
        [STAThread]
        public static void Main()
        {
            FormManager.start();
        }
    }
}
