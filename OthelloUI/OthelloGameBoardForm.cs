﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Forms;
using OthelloLogic;

namespace OthelloUI
{
    internal sealed class OthelloGameBoardForm : Form
    {
        private static GameBoardButton[,] s_OthelloBoard;
        private static List<CellCoordinates> s_PossibleCoordinates;
        private static bool s_IsBlackTurn;
        private readonly int r_BoardRib;
        private readonly bool r_IsModePlayerVsPlayer;

        public OthelloGameBoardForm(GameEventArgs i_StartEventArgs)
        {
            r_BoardRib = i_StartEventArgs.BoardRib;
            r_IsModePlayerVsPlayer = i_StartEventArgs.IsModePvsP;
            OthelloBoard = new GameBoardButton[i_StartEventArgs.BoardRib, i_StartEventArgs.BoardRib];
            s_IsBlackTurn = true;

            Text = "Othello Game";

            if (BoardRib != 12)
            {
                StartPosition = FormStartPosition.CenterScreen;
            }
        }

        public static GameBoardButton[,] OthelloBoard
        {
            get { return s_OthelloBoard; }
            set { s_OthelloBoard = value; }
        }

        public int BoardRib
        {
            get { return r_BoardRib; }
        }

        public bool IsModePlayerVsPlayer
        {
            get { return r_IsModePlayerVsPlayer; }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            initGameBoardForm();
        }

        private static void showPosibleMovesOnForm(List<CellCoordinates> i_PossibleMoves)
        {
            foreach (var cell in i_PossibleMoves)
            {
                GameBoardButton boardButton = OthelloBoard[cell.Row - 1, cell.Col - 1];
                boardButton.Enabled = true;
                boardButton.BackColor = Color.DarkSlateGray;
            }
        }

        private static void showFlippedMovesOnForm(List<CellCoordinates> i_FlipMoves)
        {
            foreach (var cell in i_FlipMoves)
            {
                GameBoardButton boardButton = OthelloBoard[cell.Row - 1, cell.Col - 1];
                boardButton.Text = "O";

                if (s_IsBlackTurn)
                {
                    boardButton.BackColor = Color.Black;
                    boardButton.ForeColor = Color.White;
                }
                else
                {
                    boardButton.BackColor = Color.White;
                    boardButton.ForeColor = Color.Black;
                }
            }
        }

        private void initGameBoardForm()
        {
            int leftSize = 5;
            int topSize = 5;

            for (int i = 0; i < BoardRib; i++)
            {
                for (int j = 0; j < BoardRib; j++)
                {
                    OthelloBoard[i, j] = new GameBoardButton(i, j)
                    {
                        Size = new Size(35, 35),
                        Location = new Point(leftSize, topSize),
                        Enabled = false
                    };

                    Controls.Add(OthelloBoard[i, j]);
                    leftSize += OthelloBoard[i, j].Width + 6;
                }

                leftSize = 5;
                topSize += 17 + 30;
            }

            MaximizeBox = false;
            this.AutoSize = true;
            this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;

            List<GameBoardButton> whiteDefaulButtons = new List<GameBoardButton>
            {
                OthelloBoard[(BoardRib / 2) - 1, (BoardRib / 2) - 1],
                OthelloBoard[(BoardRib / 2), (BoardRib / 2)]
            };

            foreach (var button in whiteDefaulButtons)
            {
                button.Enabled = true;
                button.BackColor = Color.White;
                button.Text = "O";
            }

            List<GameBoardButton> blackDefaulButtons = new List<GameBoardButton>
            {
                OthelloBoard[(BoardRib / 2) - 1, (BoardRib / 2)],
                OthelloBoard[(BoardRib / 2), (BoardRib / 2) - 1]
            };

            foreach (var button in blackDefaulButtons)
            {
                button.Enabled = true;
                button.BackColor = Color.Black;
                button.ForeColor = Color.White;
                button.Text = "O";
            }
        }

        internal void GameBoardForm_Shown(object sender, EventArgs e)
        {
            GameEngine.SupportOteloBoard.SupportBoardResetToDefault();
            s_PossibleCoordinates = GameEngine.GetPossibleMoves(s_IsBlackTurn);
            addPossibleMovesClickable(s_PossibleCoordinates);
            showPosibleMovesOnForm(s_PossibleCoordinates);
        }

        private void resetPossibleButtons(GameBoardButton i_ChosenButton)
        {
            foreach (var cell in s_PossibleCoordinates)
            {
                OthelloBoard[cell.Row - 1, cell.Col - 1].Click -= possibleButton_Clicked;
                OthelloBoard[cell.Row - 1, cell.Col - 1].BackColor = DefaultBackColor;
                OthelloBoard[cell.Row - 1, cell.Col - 1].Enabled = false;
            }

            i_ChosenButton.Enabled = true;
        }

        private void addPossibleMovesClickable(List<CellCoordinates> i_ListOfPossibles)
        {
            foreach (var cell in i_ListOfPossibles)
            {
                OthelloBoard[cell.Row - 1, cell.Col - 1].Click += possibleButton_Clicked;
            }
        }

        private void generateCompMove()
        {
            Random randomAi = new Random();
            CellCoordinates coord = s_PossibleCoordinates[randomAi.Next(s_PossibleCoordinates.Count)];
            s_OthelloBoard[coord.Row - 1, coord.Col - 1].PerformClick();
        }

        private void possibleButton_Clicked(object sender, EventArgs e)
        {
            GameBoardButton button = sender as GameBoardButton;
            resetPossibleButtons(button);
            List<CellCoordinates> flippedList = GameEngine.GetFlippedList(button.Row + 1, button.Col + 1, s_IsBlackTurn);

            showFlippedMovesOnForm(flippedList);
            s_PossibleCoordinates.Clear();
            s_IsBlackTurn = !s_IsBlackTurn;
            s_PossibleCoordinates = GameEngine.GetPossibleMoves(s_IsBlackTurn);
            if (s_PossibleCoordinates.Count == 0)
            {
                GameEngine.CalculateFinalGameScore();
                if (!GameEngine.IsFullBoard())
                {
                    MessageBox.Show(string.Format("{0}. No Possible Moves (skip turn...)", s_IsBlackTurn ? "Black" : "White"), "No Possible Moves", MessageBoxButtons.OK);
                }

                s_IsBlackTurn = !s_IsBlackTurn;
                s_PossibleCoordinates = GameEngine.GetPossibleMoves(s_IsBlackTurn);
                if (s_PossibleCoordinates.Count == 0)
                {
                    GameEngine.CalculateFinalGameScore();
                    GameEngine.SetRoundScore();
                    string winner = "Tie !!!";
                    if (GameEngine.BlackCount != GameEngine.WhiteCount)
                    {
                        winner = GameEngine.BlackCount > GameEngine.WhiteCount ? "Black Won !!!" : "White Won !!!";
                    }

                    string msg = string.Format(
@"{0} ({1}/{2})  ({3}/{4})
Would you like another round ?",
winner,
GameEngine.BlackCount,
GameEngine.WhiteCount,
GameEngine.BlackWins,
GameEngine.WhiteWins);
                    DialogResult finalDialog = MessageBox.Show(msg, "Game Over", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    checkIfcontinueForm(finalDialog);
                }
            }

            addPossibleMovesClickable(s_PossibleCoordinates);
            showPosibleMovesOnForm(s_PossibleCoordinates);
            if (!IsModePlayerVsPlayer && !s_IsBlackTurn && s_PossibleCoordinates.Count != 0)
            {
                generateCompMove();
            }
        }

        private void checkIfcontinueForm(DialogResult i_FormResult)
        {
            if (i_FormResult == DialogResult.No)
            {
                this.Close();
            }
            else
            {
                s_IsBlackTurn = !s_IsBlackTurn;
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}
