﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OthelloLogic
{
    /// <summary>
    /// Class that contains Support Board for game logic
    /// </summary>
    public sealed class SupportGameBoard
    {
        public const char k_EmptySign = ' ';
        public const char k_StartMoveSign = 'X';
        public const char k_SecondMoveSign = 'O';
        public const char k_WallSign = '#';
        private byte m_BoardRibSize;
        private char[,] m_GameBoardMatrix;

        public byte BoardRibSize
        {
            get { return m_BoardRibSize; }
            set { m_BoardRibSize = value; }
        }

        public char[,] GameBoardMatrix
        {
            get { return m_GameBoardMatrix; }
            set { m_GameBoardMatrix = value; }
        }

        internal enum eDirectionOnBoard
        {
            Up = 1,
            UpRight = 2,
            Right = 3,
            DownRight = 4,
            Down = 5,
            DownLeft = 6,
            Left = 7,
            UpLeft = 8,
        }

        internal static void InitDirectionOffsetOnBoard(eDirectionOnBoard i_Direction, out sbyte o_OffsetY, out sbyte o_OffsetX)
        {
            switch (i_Direction)
            {
                case eDirectionOnBoard.Up:
                    {
                        o_OffsetY = -1;
                        o_OffsetX = 0;
                        break;
                    }

                case eDirectionOnBoard.UpRight:
                    {
                        o_OffsetY = -1;
                        o_OffsetX = 1;
                        break;
                    }

                case eDirectionOnBoard.Right:
                    {
                        o_OffsetY = 0;
                        o_OffsetX = 1;
                        break;
                    }

                case eDirectionOnBoard.DownRight:
                    {
                        o_OffsetY = 1;
                        o_OffsetX = 1;
                        break;
                    }

                case eDirectionOnBoard.Down:
                    {
                        o_OffsetY = 1;
                        o_OffsetX = 0;
                        break;
                    }

                case eDirectionOnBoard.DownLeft:
                    {
                        o_OffsetY = 1;
                        o_OffsetX = -1;
                        break;
                    }

                case eDirectionOnBoard.Left:
                    {
                        o_OffsetY = 0;
                        o_OffsetX = -1;
                        break;
                    }

                case eDirectionOnBoard.UpLeft:
                    {
                        o_OffsetY = -1;
                        o_OffsetX = -1;
                        break;
                    }

                default:
                    {
                        o_OffsetY = 0;
                        o_OffsetX = 0;
                        break;
                    }
            }
        }

        public void SupportBoardResetToDefault()
        {
            for (byte i = 1; i <= m_BoardRibSize; i++)
            {
                for (byte j = 1; j <= m_BoardRibSize; j++)
                {
                    m_GameBoardMatrix[i, j] = k_EmptySign;
                }
            }

            m_GameBoardMatrix[(m_BoardRibSize / 2), (m_BoardRibSize / 2)] = k_SecondMoveSign;
            m_GameBoardMatrix[(m_BoardRibSize / 2) + 1, (m_BoardRibSize / 2) + 1] = k_SecondMoveSign;
            m_GameBoardMatrix[(m_BoardRibSize / 2), (m_BoardRibSize / 2) + 1] = k_StartMoveSign;
            m_GameBoardMatrix[(m_BoardRibSize / 2) + 1, (m_BoardRibSize / 2)] = k_StartMoveSign;
        }

        internal void BoardResetToDefault()
        {
            for (byte i = 1; i <= m_BoardRibSize; i++)
            {
                for (byte j = 1; j <= m_BoardRibSize; j++)
                {
                    m_GameBoardMatrix[i, j] = k_EmptySign;
                }
            }

            m_GameBoardMatrix[(m_BoardRibSize / 2), (m_BoardRibSize / 2)] = k_SecondMoveSign;
            m_GameBoardMatrix[(m_BoardRibSize / 2) + 1, (m_BoardRibSize / 2) + 1] = k_SecondMoveSign;
            m_GameBoardMatrix[(m_BoardRibSize / 2), (m_BoardRibSize / 2) + 1] = k_StartMoveSign;
            m_GameBoardMatrix[(m_BoardRibSize / 2) + 1, (m_BoardRibSize / 2)] = k_StartMoveSign;
        }

        internal void InitializeWalls(char i_WallSign)
        {
            for (byte i = 0; i < m_BoardRibSize + 2; i++)
            {
                m_GameBoardMatrix[0, i] = i_WallSign;
            }

            for (byte i = 0; i < m_BoardRibSize + 2; i++)
            {
                m_GameBoardMatrix[m_BoardRibSize + 1, i] = i_WallSign;
            }

            for (byte i = 1; i <= m_BoardRibSize; i++)
            {
                m_GameBoardMatrix[i, 0] = i_WallSign;
            }

            for (byte i = 1; i <= m_BoardRibSize; i++)
            {
                m_GameBoardMatrix[i, m_BoardRibSize + 1] = i_WallSign;
            }
        }
    }
}
