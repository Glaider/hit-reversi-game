﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using System.Text;

namespace OthelloLogic
{
    /// <summary>
    /// Static Class that contains static functions which are responsible for the game logic
    /// </summary>
    public static class GameEngine
    {


        private static readonly SupportGameBoard r_SupportOteloBoard = new SupportGameBoard();
        private static int s_BlackWins, s_WhiteWins, s_WhiteCount, s_BlackCount;

        public static SupportGameBoard SupportOteloBoard
        {
            get { return r_SupportOteloBoard; }        
        }

        public static int WhiteWins
        {
            get { return s_WhiteWins; }
            set { s_WhiteWins = value; }
        }

        public static int BlackWins
        {
            get { return s_BlackWins; }
            set { s_BlackWins = value; }
        }

        public static int WhiteCount
        {
            get { return s_WhiteCount; }
            set { s_WhiteCount = value; }
        }

        public static int BlackCount
        {
            get { return s_BlackCount; }
            set { s_BlackCount = value; }
        }

        public static void SetRoundScore()
        {
            if (BlackCount > WhiteCount)
            {
                BlackWins++;
            }
            else if (WhiteCount > BlackCount)
            {
                WhiteWins++;
            }
        }

        public static void CalculateFinalGameScore()
        {
            s_BlackCount = 0;
            s_WhiteCount = 0;
            for (byte i = 1; i <= SupportOteloBoard.BoardRibSize; i++)
            {
                for (byte j = 1; j <= SupportOteloBoard.BoardRibSize; j++)
                {
                    if (r_SupportOteloBoard.GameBoardMatrix[i, j] == 'X')
                    {
                        BlackCount++;
                    }
                    else if (r_SupportOteloBoard.GameBoardMatrix[i, j] == 'O')
                    {
                        WhiteCount++;
                    }
                }
            }
        }

        public static bool IsFullBoard()
        {
            return BlackCount + WhiteCount ==
                   SupportOteloBoard.BoardRibSize * SupportOteloBoard.BoardRibSize;
        }

        public static List<CellCoordinates> GetPossibleMoves(bool i_IsBlackMove)
        {
            char mark = i_IsBlackMove ? SupportGameBoard.k_StartMoveSign : SupportGameBoard.k_SecondMoveSign;
            return getMooves(mark, r_SupportOteloBoard);
        }

        private static List<CellCoordinates> getMooves(char i_Mark, SupportGameBoard i_Board)
        {
            List<CellCoordinates> listOfPossible = new List<CellCoordinates>();

            for (byte i = 1; i <= i_Board.BoardRibSize; i++)
            {
                for (byte j = 1; j <= i_Board.BoardRibSize; j++)
                {
                    // Find all the markings of a player on the board
                    if (i_Board.GameBoardMatrix[i, j] == i_Mark)
                    {
                        // Check all possible directions to make moves
                        foreach (SupportGameBoard.eDirectionOnBoard direction in Enum.GetValues(typeof(SupportGameBoard.eDirectionOnBoard)))
                        {
                            sbyte offsetY;
                            sbyte offsetX;
                            SupportGameBoard.InitDirectionOffsetOnBoard(direction, out offsetY, out offsetX);
                            recursivelyCheckPossibleMovesInGivenDirection(i_Board, i_Mark, i, j, offsetY, offsetX, listOfPossible);
                        }
                    }
                }
            }

            return listOfPossible;
        }

        private static void recursivelyCheckPossibleMovesInGivenDirection(SupportGameBoard i_Board, char i_Mark, byte i_Y, byte i_X, sbyte i_YOffset, sbyte i_XOffset, List<CellCoordinates> io_ListWithPosible, bool i_AtleastOneRecursion = false)
        {
            char oponentMark = i_Mark == SupportGameBoard.k_StartMoveSign ? SupportGameBoard.k_SecondMoveSign : SupportGameBoard.k_StartMoveSign;
            char besideMarkInGivenDirection = i_Board.GameBoardMatrix[i_Y + i_YOffset, i_X + i_XOffset];
            if (besideMarkInGivenDirection == oponentMark)
            {
                const bool v_AtleastOneRecursion = true;
                recursivelyCheckPossibleMovesInGivenDirection(i_Board, i_Mark, (byte)(i_Y + i_YOffset), (byte)(i_X + i_XOffset), i_YOffset, i_XOffset, io_ListWithPosible, v_AtleastOneRecursion);
            }
            else if (besideMarkInGivenDirection == ' ' && i_AtleastOneRecursion)
            {
                CellCoordinates possibleCell = new CellCoordinates((byte)(i_Y + i_YOffset), (byte)(i_X + i_XOffset));
                if (!io_ListWithPosible.Contains(possibleCell))
                {
                    io_ListWithPosible.Add(possibleCell);
                }
            }
        }

        public static void InitSupportBoard()
        {
            SupportOteloBoard.BoardResetToDefault();
            SupportOteloBoard.InitializeWalls(SupportGameBoard.k_WallSign);
        }

        private static byte recursivelyReverseOpponentMarks(char i_PlayerMark, byte i_NumberOfPlacedMark, byte i_LetterOfPlacedMark, sbyte i_OffsetY, sbyte i_OffsetX, SupportGameBoard i_Board, List<CellCoordinates> i_FlipedList, bool i_IsCanFlip = false)
        {         
            byte flippedMarks = 0;
            char markToFlip = i_PlayerMark == SupportGameBoard.k_StartMoveSign ? SupportGameBoard.k_SecondMoveSign : SupportGameBoard.k_StartMoveSign;
            char besideMarkInGivenDirection = i_Board.GameBoardMatrix[i_NumberOfPlacedMark + i_OffsetY, i_LetterOfPlacedMark + i_OffsetX];
            if (besideMarkInGivenDirection == markToFlip)
            {
                const bool v_AtleastOneOpponentMark = true;
                flippedMarks += recursivelyReverseOpponentMarks(i_PlayerMark, (byte)(i_NumberOfPlacedMark + i_OffsetY), (byte)(i_LetterOfPlacedMark + i_OffsetX), i_OffsetY, i_OffsetX, i_Board, i_FlipedList, v_AtleastOneOpponentMark);
            }

            if ((besideMarkInGivenDirection == i_PlayerMark && i_IsCanFlip) || (flippedMarks > 0 && i_Board.GameBoardMatrix[i_NumberOfPlacedMark, i_LetterOfPlacedMark] != i_PlayerMark))
            {
                i_Board.GameBoardMatrix[i_NumberOfPlacedMark, i_LetterOfPlacedMark] = i_PlayerMark;
                i_FlipedList.Add(new CellCoordinates(i_NumberOfPlacedMark, i_LetterOfPlacedMark));
                flippedMarks++;
            }

            return flippedMarks;
        }

        public static List<CellCoordinates> GetFlippedList(int i_Row, int i_Col, bool i_IsBlackTurn)
        {
            char mark = i_IsBlackTurn ? 'X' : 'O';
            List<CellCoordinates> flipList = new List<CellCoordinates>();

            r_SupportOteloBoard.GameBoardMatrix[i_Row, i_Col] = mark;
            
            foreach (SupportGameBoard.eDirectionOnBoard direction in Enum.GetValues(typeof(SupportGameBoard.eDirectionOnBoard)))
            {
                sbyte offsetY;
                sbyte offsetX;
                SupportGameBoard.InitDirectionOffsetOnBoard(direction, out offsetY, out offsetX);
                recursivelyReverseOpponentMarks(mark, (byte)i_Row, (byte)i_Col, offsetY, offsetX, r_SupportOteloBoard, flipList);
            }

            flipList.Add(new CellCoordinates((byte)i_Row, (byte)i_Col));
            return flipList;
        }     
    }
}
