﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OthelloLogic
{
    /// <summary>
    /// Structure contains cell coordinates
    /// </summary>
     public struct CellCoordinates
        {
            private readonly byte r_Row;
            private readonly byte r_Col;

            internal CellCoordinates(byte i_Row, byte i_Col)
            {
                r_Row = i_Row;
                r_Col = i_Col;
            }

            public byte Row
            {
                get { return r_Row; } 
            }

            public byte Col
            {
                get { return r_Col; }             
            }
        }
}
